/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.midterm;

import java.util.Scanner;

/**
 *
 * @author Oil
 */
public class Ex1 {
    static void function (int arr[], int C){
        for(int i = 0; i<arr.length; i++){
            for(int j = i+1; j<arr.length; j++){
                if(arr[i]+arr[j]==C){
                    System.out.println(arr[i]+"+"+arr[j]+"="+C);
                }
            }
        }
    }
    public static void main(String[]args){
        Scanner kb = new Scanner (System.in);
        
        int c = kb.nextInt();
        int size = kb.nextInt();
        
        int[]arr = new int[size];
        
        for(int i = 0; i<size; i++){
            arr[i] = kb.nextInt();
        }
        function(arr,c);
    }
}
